let hora = 0;
let minuto = 0;
let segundo = 0;
let centesima = 0;
let tiempo = 0;
let status = false;

function incHora() {
    hora = hora + 1;
    if (hora < 10) {
        horas.innerHTML = "0" + hora;
    } else {
        horas.innerHTML = hora;
    }
}

function decHora() {
    hora = hora - 1;
    if (hora < 10) {
        if (hora <= 0) {
            hora = 0;
            horas.innerHTML = "00";
        } else {
            horas.innerHTML = "0" + hora;
        }
    } else {
        horas.innerHTML = hora;
    }
}

function incMin() {
    minuto = minuto + 1;
    if (minuto == 60) {
        minuto = 0;
        minutos.innerHTML = "00";
        hora = hora + 1;
        if (hora < 10) {
            horas.innerHTML = "0" + hora;
        } else {
            horas.innerHTML = hora;
        }
    } else {
        if (minuto < 10) {
            minutos.innerHTML = "0" + minuto;
        } else {
            minutos.innerHTML = minuto;
        }
    }
}

function decMin() {
    minuto = minuto - 1;
    if (minuto > 0 && minuto < 10) {
        minutos.innerHTML = "0" + minuto;
    } else if (minuto < 0 && hora != 0) {
        minuto = 59;
        minutos.innerHTML = "59";
        hora = hora - 1;
        if (hora < 10) {
            if (hora <= 0) {
                hora = 0;
                horas.innerHTML = "00";
            } else {
                horas.innerHTML = "0" + hora;
            }
        } else {
            minuto = 0;
            minutos.innerHTML = "00";
            horas.innerHTML = hora;
        }
    } else if (minuto <= 0 && hora == 0) {
        minuto = 0;
        minutos.innerHTML = "00";
    } else if (minuto == 0) {
        minutos.innerHTML = "00";
    } else {
        minutos.innerHTML = minuto;
    }
}

function incSeg() {
    segundo = segundo + 1;
    if (segundo == 60) {
        segundo = 0;
        segundos.innerHTML = "00";
        minuto = minuto + 1;
        if (minuto < 10) {
            minutos.innerHTML = "0" + minuto;
        } else {
            minutos.innerHTML = minuto;
        }
    } else {
        if (segundo < 10) {
            segundos.innerHTML = "0" + segundo;
        } else {
            segundos.innerHTML = segundo;
        }
    }
}

function decSeg() {
    segundo = segundo - 1;
    if (segundo > 0 && segundo < 10) {
        segundos.innerHTML = "0" + segundo;
    } else if (segundo < 0 && minuto != 0) {
        segundo = 59;
        segundos.innerHTML = "59";
        minuto = minuto - 1;
        if (minuto < 10) {
            if (minuto <= 0) {
                minuto = 0;
                minutos.innerHTML = "00";
            } else {
                minutos.innerHTML = "0" + minuto;
            }
        } else {
            segundo = 0;
            segundos.innerHTML = "00";
            minutos.innerHTML = minuto;
        }
    } else if (segundo <= 0 && minuto == 0) {
        segundo = 0;
        segundos.innerHTML = "00";
    } else if (segundo == 0) {
        segundos.innerHTML = "00";
    } else {
        segundos.innerHTML = segundo;
    }
}

function incCent() {
    centesima = centesima + 1;
    if (centesima == 100) {
        centesima = 0;
        centesimas.innerHTML = "00";
        segundo = segundo + 1;
        if (segundo < 10) {
            segundos.innerHTML = "0" + segundo;
        } else {
            segundos.innerHTML = segundo;
        }
    } else {
        if (centesima < 10) {
            centesimas.innerHTML = "0" + centesima;
        } else {
            centesimas.innerHTML = centesima;
        }
    }
}

function decCent() {
    centesima = centesima - 1;
    if (centesima >= 10) {
        centesimas.innerHTML = centesima;
    } else {
        if (centesima >= 0 && centesima < 10) {
            centesimas.innerHTML = "0" + centesima;
        } else if (centesima < 0 && segundo == 00 && minuto == 0 && hora == 0) {
            centesima = 0;
            centesimas.innerHTML = "00";
        } else if (centesima < 0 && segundo != 0) {
            segundo = segundo - 1;
            if (segundo >= 0 && segundo < 10) {
                segundos.innerHTML = "0" + segundo;
            } else {
                segundos.innerHTML = segundo;
            }
            centesima = 99;
            centesimas.innerHTML = centesima;
        } else if (centesima < 0 && segundo == 0 && minuto != 0) {
            minuto = minuto - 1;
            if (minuto >= 0 && minuto < 10) {
                minutos.innerHTML = "0" + minuto;
            } else {
                minutos.innerHTML = minuto;
            }
            segundo = 59;
            segundos.innerHTML = segundo;
            centesima = 99;
            centesimas.innerHTML = centesima;
        } else if (centesima < 0 && segundo == 0 && minuto == 0 && hora != 0) {
            hora = hora - 1;
            if (hora >= 0 && hora < 10) {
                horas.innerHTML = "0" + hora;
            } else {
                horas.innerHTML = "00";
            }
            minuto = 59;
            minutos.innerHTML = minuto;
            segundo = 59;
            segundos.innerHTML = segundo;
            centesima = 99;
            centesimas.innerHTML = centesima;
        }
    }
}


function start() {
    let tiempo = (hora * 360000) + (minuto * 6000) + (segundo * 100) + centesima
    console.log(tiempo)
    if (tiempo != 0) {
        status = true;
        comenzar = setInterval(decCent, 10);
        window.setTimeout(stop, tiempo*10+10);
        window.setTimeout(restart, tiempo*10+20);
    }
}

function stop() {
    if (status == true) {
        status = false;
        window.clearInterval(comenzar);
    }
}

function restart() {
    if (status == false) {
        hora = 0;
        minuto = 0;
        segundo = 0;
        centesima = 0;
        horas.innerHTML = "00";
        minutos.innerHTML = "00";
        segundos.innerHTML = "00";
        centesimas.innerHTML = "00";
    }
}
